﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

public class AutoLinkEyeOnTarget : MonoBehaviour
{
    public Transform m_toSetAsTarget;
    public bool m_setAsMainOnAwake = true;
    public bool m_setAsMainOnStart = true;
    public bool m_setAsMainOnEnable=true;
    public bool m_override;

    private void Start()
    {
        if (m_setAsMainOnStart)
            SetThisAsMainTargetOfEyesInScene();
    }
    private void Awake()
    {
        if (m_setAsMainOnAwake)
            SetThisAsMainTargetOfEyesInScene();
    }

    private void OnEnable()
    {
        if (m_setAsMainOnEnable)
            SetThisAsMainTargetOfEyesInScene();
    }
    public void SetThisAsMainTargetOfEyesInScene() {
        EyesTarget.SetAsMainTarget(m_toSetAsTarget, m_override);
    }
    private void Reset()
    {
        m_toSetAsTarget = transform;
    }
}
