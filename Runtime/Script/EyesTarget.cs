﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyesTarget : MonoBehaviour
{
    private static Transform m_mainTarget;
    private static List<Transform> m_targets;

    public static Transform GetMainTarget() { return m_mainTarget; }
    public static List<Transform> GetRegisteredTargets() { return m_targets; }

    public static void AddAsRegisteredTarget(Transform target) {
        m_targets.Remove(target);
        m_targets.Add(target);
    }
    public static void RemoveAsRegisteredTarget(Transform target) {
        m_targets.Remove(target);
    }

    public static void SetAsMainTarget(Transform target, bool overrideTarget=true, bool replaceIfMainNotActive=true) {
        if (replaceIfMainNotActive && m_mainTarget!=null &&!m_mainTarget.gameObject.activeInHierarchy)
            m_mainTarget = target;
        if(m_mainTarget==null || (overrideTarget) )
          m_mainTarget = target;
    }

}
