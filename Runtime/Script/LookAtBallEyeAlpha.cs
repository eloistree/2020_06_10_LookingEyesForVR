﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtBallEyeAlpha : MonoBehaviour
{
    public Transform m_eyeContainerDirection;
    public Transform m_toAffect;
    public Transform m_target;

    public bool m_allowRawRotation = false;
    public bool m_useLerp=true;
    public float m_lerpFactor=2f;
    public bool m_useClampRotation = true;
    [Range(0, 180)]
    public float m_horizontal = 90;
    [Range(0,180)]
    public float m_vertical = 30;
    public bool m_autoForMainTarget=true;
    [Header("Debug")]
    public Vector3 m_currentPositionLooked;
    public Vector3 m_wantedPositionLooked;
    public Vector3 m_localRotationEuler;
    public void Update()
    {
        if (m_target == null && m_autoForMainTarget)
            m_target = EyesTarget.GetMainTarget();

        if (m_target)
            m_wantedPositionLooked = m_target.position;

        Vector3 upDirection = Vector3.up;
        if (m_eyeContainerDirection == null)
            m_eyeContainerDirection = m_toAffect.parent;
        if (m_eyeContainerDirection != null)
            upDirection = m_eyeContainerDirection.up;

        if (!m_useLerp) { 
            m_currentPositionLooked= m_wantedPositionLooked;
        }
        else {
            m_currentPositionLooked = Vector3.Lerp(m_currentPositionLooked, m_wantedPositionLooked, Time.deltaTime * m_lerpFactor);
        }
        m_toAffect.LookAt(m_currentPositionLooked, upDirection);

        Quaternion localRotation = m_toAffect.localRotation;
        Vector3 euler = localRotation.eulerAngles;
        euler.x = euler.x % 360f;
        euler.y = euler.y % 360f;
        if (euler.x > 180f) 
            euler.x = (euler.x - 360);
        if (euler.y > 180f) 
            euler.y = (euler.y - 360);
        m_localRotationEuler = euler;
        euler.x = Mathf.Clamp(euler.x, -m_vertical, m_vertical);
        euler.y = Mathf.Clamp(euler.y, -m_horizontal, m_horizontal);
        m_toAffect.localRotation = Quaternion.Euler(euler);
    }


    public void SetTarget(Transform target) {
        m_target = target;
    }
}
